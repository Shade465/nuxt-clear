export default {
    mode: 'universal',
    /*
     ** Headers of the page
     */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || ''
            }
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },
    /*
     ** Global CSS
     */
    css: ['~/assets/css/main.css'],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: ['~/plugins/scroll-lock.client.js'],

    router: {
        linkExactActiveClass: 'active'
    },
    /*
     ** Nuxt.js modules
     */
    devModules: ['@nuxtjs/eslint-module'],
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extractCSS: false,
        analyze: false,
        postcss: {
            plugins: {
                'postcss-color-mod-function': {},
                'postcss-custom-media': {
                    importFrom: ['./assets/css/media.css']
                }
            },
            preset: {
                stage: 0
            }
        },
        extend(config, ctx) {}
    }
}
